### Steps to run

- Install dependencies by composer with __composer install__
- Copy .env.example to .env
- run __php artisan key:generate__
- run __php artisan config:cache__
- run WebServer __php artisan serve --port=8080__
#### Warning: 
- You  need to run WEB on port 8080 so you do not conflict with the API
- __php artisan serve --port=8080__