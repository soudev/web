<?php
/**
 * Created by PhpStorm.
 * User: soudev
 * Date: 29/04/18
 * Time: 09:35
 */

namespace App\Http\Controllers\Doctors;

use App\Http\Client\DoctorClient;
use App\Http\Client\ExpertisesClient;
use App\Http\Controllers\Controller;
use App\DoctorsImagesModel;
use App\DoctorsModel;


class DoctorsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $expertises = new ExpertisesClient();

        return view('doctors/index', ['expertises'=>$expertises->getExpertises()]);
    }
}