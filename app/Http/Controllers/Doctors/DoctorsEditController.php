<?php


namespace App\Http\Controllers\Doctors;

use App\Http\Client\DoctorClient;
use App\Http\Client\ExpertisesClient;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\RequestException;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Http\Request;

/**
 * Class DoctorsEditController
 * @package App\Http\Controllers\Doctors
 */
class DoctorsEditController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $client;

    /**
     * DoctorsEditController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->client = new DoctorClient;
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {

        $data = $this->client->getDoctor($id);

        $expertises = new ExpertisesClient;
        return view('doctors/update', ['data' => $data, 'doctor_expertises'=>$expertises->getExpertisesId($data->expertises), 'expertises'=> $expertises->getExpertises()]);

    }

}