<?php


namespace App\Http\Controllers\Doctors;

use App\Http\Client\DoctorClient;
use App\Http\Controllers\Controller;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Http\Request;

/**
 * Class DoctorsListController
 * @package App\Http\Controllers\Doctors
 */
class DoctorsListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $client;

    /**
     * DoctorsListController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->client = new DoctorClient;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getList(Request $request)
    {

        $data = $this->client->getDoctors();
        return view('doctors/list', ['data' => $data]);

    }


}