<?php


namespace App\Http\Controllers\Doctors;

use App\Http\Client\DoctorClient;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\RequestException;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Http\Request;

/**
 * Class DoctorsUpdateController
 * @package App\Http\Controllers\Doctors
 */
class DoctorsUpdateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $client;

    /**
     * DoctorsUpdateController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->client = new DoctorClient;
    }


    public function postUpdate($id, Request $request)
    {

        $data = $request->all();
        try {
            $response = $this->client->updateDoctor($id, $data);

            $msg = json_decode($response->getBody());

            return redirect()->route('doctors/edit',  ['id'=> $id])->with('success', $msg->data->msg);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                if ($e->getResponse()->getStatusCode() == 422) {
                    $errors = json_decode($e->getResponse()->getBody());
                    return redirect()->route('doctors/edit', ['id'=> $id])->with('error', 'Verifique os dados enviados')->withErrors($errors->data->msg)->withInput();
                }
                return redirect()->route('doctors/edit',  ['id'=> $id])->with('error', 'Algo saiu errado')->withInput();
            }
        }

    }

}