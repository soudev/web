<?php


namespace App\Http\Controllers\Doctors;

use App\Http\Client\DoctorClient;
use App\Http\Controllers\Controller;
use Guzzle\Http\Exception\ClientErrorResponseException;

/**
 * Class DoctorsSeeController
 * @package App\Http\Controllers\Doctors
 */
class DoctorsSeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $client;

    /**
     * DoctorsSeeController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->client = new DoctorClient;
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSee($id)
    {

        $data = $this->client->getDoctor($id);

        return view('doctors/see', ['data' => $data]);

    }


}