<?php


namespace App\Http\Controllers\Doctors;

use App\Http\Client\DoctorClient;
use App\Http\Controllers\Controller;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Http\Request;

/**
 * Class DoctorsDeleteController
 * @package App\Http\Controllers\Doctors
 */
class DoctorsDeleteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $client;

    /**
     * DoctorsDeleteController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->client = new DoctorClient;
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {

        try {

            $response = $this->client->deleteDoctor($id);

            $msg = json_decode($response->getBody());

            return redirect()->route('doctors/list')->with('success', $msg->data->msg);

        }catch (RequestException $e) {
            if ($e->hasResponse()) {
                if ($e->getResponse()->getStatusCode() == 422) {
                    $errors = json_decode($e->getResponse()->getBody());
                    return redirect()->route('doctors/list')->with('error', 'Verifique os dados enviados')->withErrors($errors->data->msg)->withInput();
                }
                return redirect()->route('doctors/list')->with('error', 'Algo saiu errado');
            }
        }
    }

}