<?php

namespace App\Http\Client;

class ExpertisesClient extends MyDoctorClient
{

    const BASE = 'expertises';


    public function getExpertises()
    {

        $response = $this->getClient()->get(self::BASE);
        $data = json_decode($response->getBody());
        return $data->data->msg;
    }

    public function getExpertisesId($data)
    {
        foreach ($data as $expertise) {
            $ids[] = $expertise->pivot->expertises_id;
        }
        return $ids;
    }
}