<?php

namespace App\Http\Client;

class DoctorClient extends MyDoctorClient
{

    const BASE = 'doctors';

    public function deleteDoctor($id){
        return $this->getClient()->delete(self::BASE . '/' . $id);
    }

    public function getDoctor($id){

        $response = $this->getClient()->get(self::BASE . '/' . $id);
        $data = json_decode($response->getBody());

        return $data->data->msg;
    }

    public function getDoctors(){

        $response = $this->getClient()->get(self::BASE);
        $data = json_decode($response->getBody());

        return $data->data->msg;

    }

    public function saveDoctor(array $data){

        return $this->getClient()->post(self::BASE, [
            'form_params' => $data
        ]);
    }

    public function updateDoctor($id, array $data){

        return $this->getClient()->put(self::BASE . '/' . $id, [
            'form_params' => $data
        ]);
    }


}