<?php
/**
 * Created by PhpStorm.
 * User: adrianolima
 * Date: 2019-04-25
 * Time: 20:12
 */
namespace App\Http\Client;
use GuzzleHttp\Client;

abstract class MyDoctorClient
{

    private $host = "http://localhost:8000/api/";
    private $client;
    protected $headers = [];
    private $endpoint;

    /**
     * MyDoctorClient constructor.
     */
    public function __construct($headers = [])
    {
        $this->client =  new \GuzzleHttp\Client(['base_uri' => $this->host]);
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return MyDoctorClient
     */
    public function setHost(string $host): MyDoctorClient
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     * @return MyDoctorClient
     */
    public function setHeaders(array $headers): MyDoctorClient
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param mixed $endpoint
     */
    public function setEndpoint($endpoint): void
    {
        $this->endpoint = $endpoint;
    }

    public function getBody()
    {
        return json_decode($this->client->getBody(), true);
    }

}