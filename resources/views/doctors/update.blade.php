@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cadastro</div>

                    <div class="panel-body">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <ul>
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            </ul>
                        @endif
                        <form class="form-horizontal" method="POST" action="{{ route('doctors/update', ['id' =>$data->id]) }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input id="id" type="hidden" class="form-control" name="id" value="{{ old('id', $data->id) }}">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="nickname" class="col-md-4 control-label">Nome</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ old('name', $data->name) }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('crm') ? ' has-error' : '' }}">
                                <label for="crm" class="col-md-4 control-label">CRM</label>
                                <div class="col-md-6">
                                    <input id="crm" type="text" class="form-control" name="crm"
                                           value="{{ old('crm', $data->crm) }}" required autofocus>
                                    @if ($errors->has('crm'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('crm') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('crm') ? ' has-error' : '' }}">
                                <label for="crm" class="col-md-4 control-label">Telefone</label>
                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control" name="phone"
                                           value="{{ old('phone', $data->phone) }}" required autofocus>
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="panel-heading"><h4 class="my-4 text-lg-left">Especialidades</h4></div>
                            <div class="form-group{{ $errors->has('expertises[]') ? ' has-error' : '' }}">
                                @foreach ($expertises as $expertise)
                                    <div class="col-md-6">
                                        <input type="checkbox" class="form-check-input m-lg-auto" id="expertises" name="expertises[]" value="{{$expertise->id}}" @if(in_array($expertise->id, (array)$doctor_expertises)) {{'checked'}} @endif>
                                        <label class="form-check-label" for="expertises">{{$expertise->expertise}}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Salvar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
