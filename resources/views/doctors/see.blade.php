@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row form-group">
                            <label for="nickname" class="col-md-4 control-label">Nome</label>
                            <div class="col-md-6 pa">
                                {{$data->name}}
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="real_name" class="col-md-4 control-label">CRM</label>
                            <div class="col-md-6">
                                {{$data->crm}}
                            </div>
                        </div>

                        <div class="row form-group">
                            <label for="origin_description" class="col-md-4 control-label">Telefone</label>
                            <div class="col-md-6">
                                {{$data->phone}}
                            </div>
                        </div>
                        @if(count($data->expertises) >0)
                            <div class="panel-heading"><h4 class="my-4 text-lg-left">Especialidades</h4></div>

                            <div class="form-group{{ $errors->has('expertises[]') ? ' has-error' : '' }}">

                                @foreach ($data->expertises as $expertise)
                                    <div class="col-md-6">
                                        <label class="form-check-label" for="expertises">{{$expertise->expertise}}</label>
                                    </div>
                                @endforeach

                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>

</script>
