@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row center-block">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <ul>
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        </ul>
                    @endif
                    <table class="table table-striped">

                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CRM</th>
                            <th >Telefone</th>
                            <th >Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($data as $entry )
                            <tr >

                                {{--<td >--}}
                                    {{--@if($entry->photo['image'])--}}
                                    {{--<img class="img-responsive img-hero img-circle" src="/{{$entry->photo['image']}}" >--}}
                                        {{--@endif--}}
                                   {{--</td>--}}
                                <td >{{$entry->name}}</td>
                                <td >{{$entry->crm}}</td>
                                <td >{{$entry->phone}}</td>
                                <td>
                                    <a href="{{ route('doctors/see', ['id' =>$entry->id]) }}">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>
                                    <a href="{{ route('doctors/edit', ['id' =>$entry->id]) }}">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a href="{{ route('doctors/delete', ['id' =>$entry->id]) }}">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr class="">
                                <td colspan="9">
                                    :( Nenhum Registro encontrado
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
