<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/doctors', 'Doctors\DoctorsController@index')->name('doctors');
Route::get('/doctors/list', 'Doctors\DoctorsListController@getList')->name('doctors/list');
Route::get('/doctors/see/{id}', 'Doctors\DoctorsSeeController@getSee')->name('doctors/see');
Route::get('/doctors/delete/{id}', 'Doctors\DoctorsDeleteController@getDelete')->name('doctors/delete');
Route::get('/doctors/edit/{id}', 'Doctors\DoctorsEditController@getEdit')->name('doctors/edit');
Route::post('/doctors/save', 'Doctors\DoctorsRegisterController@postSave')->name('doctors/save');
Route::post('/doctors/update/{id}', 'Doctors\DoctorsUpdateController@postUpdate')->name('doctors/update');

